/* NOS SIRVE PARA VALIDAR QUE EL USUARIO ESTE AUTENTICADO PARA PERMITIRLE CIERTAS ACCIONES CON CIERTA INFORMACIÓN */

// Importamos Json Web Token (JWT)
const jwt = require('jsonwebtoken')



module.exports = function( req, res, next ){
    // Leer el token del header
    const token = req.header('x-auth-token') // x-auth-token, en cada request se debe enviar

    // Revisar si no hay token 
    if( !token ){
        return res.status(401).json({ msg: 'No hay token, permiso no válido' })
    }

    // Validar el token
    try {

        // Válidamos el token con la palabra secreta que se encuentra en nuestras variables de entorno
        const encryption = jwt.verify(token, process.env.SECRET)
        req.user = encryption.user // se pasa el token a la petición que hace el usuario y que este token sea accesible desde el controller, puesto que en este token está el id del usuario, básicamente le decímos al request que tendra un objeto usuario y que ese usuario será lo que haya en encryption.user (nos ayudamos pasando el token jwt.io para validar esto que escribo)
        next() // Pasa al siguiente middleware

    } catch(error) {
        res.status(401).json({msg: 'Token no válido'})
    }

} 