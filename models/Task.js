/* CREAMOS LOS MÓDELOS (Estructura de la base de datos) */

// importamos mongoose
const mongoose = require('mongoose')


// Definimos la estructura de la DB con los schemas
const TasksSchema = mongoose.Schema({
    name: {
        type: String, // Tipo de dato aceptado
        required: true,
        trim: true
    },
    state: {
        type: Boolean, // Tipo de dato aceptado
        default: false
    },
    dateCreated: {
        type: Date, // Tipo de dato aceptado
        default: Date.now()
    },
    project: {
        type: mongoose.Schema.Types.ObjectId, // En este caso el tipo de dato será un id de el proyecto en el que se encuentra
        ref: 'Project' // Referencia del módelo
    }
})

// Exportamos el módelo
module.exports = mongoose.model('Task', TasksSchema) // model tiene 2 parámetros (nombre que le daremos al módelo, el schema del módelo)