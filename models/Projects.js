/* CREAMOS LOS MÓDELOS (Estructura de la base de datos) */

// Importamos mongoose
const mongoose = require('mongoose')



// Definimos la estructura de la DB con los schemas
const ProjectsSchema = mongoose.Schema({
    name: {
        type: String, // Tipo de dato aceptado
        required: true,
        trim: true
    },
    creator: {
        type: mongoose.Schema.Types.ObjectId, // el id del creador
        rel: 'Project' // Debe ser el nombre del módelo para asociar ese id 
    },
    creationDate: {
        type: Date, // Tipo de dato aceptado
        default: Date.now() // Selecciona la fecha del registro
    }
})

module.exports = mongoose.model('Project', ProjectsSchema) // model tiene 2 parámetros (nombre que le daremos al módelo, el schema del módelo)