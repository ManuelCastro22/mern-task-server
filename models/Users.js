/* CREAMOS LOS MÓDELOS (Estructura de la base de datos) */

// Importamos mongoose
const mongoose = require('mongoose')



// Definimos la estructura de la DB con los schemas
const UsersSchema = mongoose.Schema({
    name: {
        type: String, // Tipo de dato aceptado
        required: true,
        trim: true
    },
    email: {
        type: String, // Tipo de dato aceptado
        required: true,
        trim: true,
        unique: true // Que sea único, puesto que no hay email repetido
    },
    pass: {
        type: String, // Tipo de dato aceptado
        required: true,
        trim: true
    },
    dateRegister: {
        type: Date, // Tipo de dato aceptado
        default: Date.now() // Selecciona la fecha del registro
    }
})

// Exportamos el módelo
module.exports = mongoose.model('User', UsersSchema) // model tiene 2 parámetros (nombre que le daremos al módelo, el schema del módelo)