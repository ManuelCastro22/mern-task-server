// Configuración de la Base de datos

// Importamos mongoose para hacer la consulta
const mongoose = require('mongoose')
// Importamos dotenv, que permite leer las variables de entorno, puesto que estás cambian en local y en producción
require('dotenv').config({ path: 'variables.env' })



// Conectar DB
const conectDB = async () => {
    try {
        await mongoose.connect(process.env.DB_MONGO, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: true
        }) // Toma como primer parámetro la URL a la cuál se va a conectar (en este caso nos servimos de 'dotenv' para usar las variables de entorno en donde se encuentra la url) y como segundo la configuración
        console.log('DB conected') // Cuando se conecta exitosamente
    } catch( error ){
        console.log(error) // Muestra el error
        process.exit(1) // Detiene la app
    }
}

module.exports = conectDB // Exportamos la función de conexión