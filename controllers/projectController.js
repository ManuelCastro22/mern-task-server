/* TENDRÁ MÉTODOS RELACIONADOS A UNOS ENDPOINTS */

// Importamos los módelos 
const Project = require('../models/Projects')
// Importamos express-validator, los resultaos de las validaciones
const { validationResult } = require('express-validator') // importamos la función validationResult


// Función para crear un nuevo proyecto
exports.createProject = async (req, res) => {

    // Revisamos si hay errores en la validación
    const errors =  validationResult(req)
    if( !errors.isEmpty() ){
        return res.status(400).json({ errors: errors.array() })
    }

    try {

        // Creando un nuevo proyecto con lo que venga en el request
        const project = new Project(req.body) // Con la palabra new se le especifica que es un nuevo proyecto

        // Guardar el creador vía JWT
        project.creator = req.user.id // El usuario que este autenticado y que se pasará en la consulta

        // Guarda el nuevo proyecto
        await project.save()

        // Mensaje de Confirmación
        res.json(project)

    } catch(error) {
        console.log(error)
        res.status(500).send('hubo un error')
    }

}

// Función para obtener los proyectos del usuario actual
exports.getProjects = async(req, res) => {

    try {

        // Almacena los proyectos que se obtienen del módelo que tengan como creador el usuario pasado en el token del request
        const projects = await Project.find({ creator: req.user.id }).sort( { creationTime: -1 } ) // .find() es un método de mongoose permite buscar un elemento que cumpla con el parámetro y devuelve varios, con .sort() ordernamos los proyectos por un parámetro en este caso por el orden de fecha
        res.json({ projects })

    } catch (error) {
        console.log(error)
        res.status(500).send('hubo un error')
    }

}

// Función para actualizar un proyecto
exports.updateProject = async(req, res) => {

    // Revisamos si hay errores en la validación
    const errors =  validationResult(req)
    if( !errors.isEmpty() ){
        return res.status(400).json({ errors: errors.array() })
    }

    // Extraemos la información del proyecto del request
    const { name } = req.body,
        newProject = {} // Objeto nuevo temporal para sobreescribir la nueva data que venga

    // Validamos que existan los campos a actualizar
    if( name ) {
        newProject.name = name
    }

    try {
        
        // console.log(req.params.id), con "req.params.id" podemos ver que nos envian como id
        // Revisar el Id
        let project = await Project.findById(req.params.id) // busca adentro del módelo un proyecto que coincida con el id, siempre que se hace una consulta a la DB se debe usar await

        // Si el proyecto existe o no
        if( !project ) {
            return res.status(404).json({ msg: "Proyecto no encontrado" })
        }

        // Verificar que el creador del proyecto sea la misma persona que está autenticada
        if( project.creator.toString() !== req.user.id ) { // Convertimos el valor de creator que se encuentra almacenado como objeto en el módelo a string y lo comparamos con el parámetro con el id del usuario del request
            return res.status(401).json({ msg: "No autorizado" })
        }

        // Actualizar
        project = await Project.findByIdAndUpdate({ _id: req.params.id }, { $set: newProject }, { new: true }) // Busca por id y actualiza, params: 1. id del proyecto que se va a actualizar, 2. con que se va a actualizar es decir la nueva data, 3 confirmamos
        
        // Retornamos la respuesta de la DB
        res.json({ project })

    } catch(error){
        console.log(error)
        res.status(500).send('hubo un error')
    }

}

// Función para eliminar un proyecto
exports.deleteProject = async(req, res) => {
        
    try {

        // console.log(req.params.id), con "req.params.id" podemos ver que nos envian como id
        // Revisar el Id
        let project = await Project.findById(req.params.id) // busca adentro del módelo un proyecto que coincida con el id, siempre que se hace una consulta a la DB se debe usar await

        // Si el proyecto existe o no
        if( !project ) {
            return res.status(404).json({ msg: "Proyecto no encontrado" })
        }

        // Verificar que el creador del proyecto sea la misma persona que está autenticada
        if( project.creator.toString() !== req.user.id ) { // Convertimos el valor de creator que se encuentra almacenado como objeto en el módelo a string y lo comparamos con el parámetro con el id del usuario del request
            return res.status(401).json({ msg: "No autorizado" })
        }

        // Eliminar el proyecto
        await Project.findOneAndRemove( { _id: req.params.id } )

        // Retornamos la respuesta de la DB
        res.json({ msg: "Proyecto Eliminado" })

    } catch(error){
        console.log(error)
        res.status(500).send('Error en el error')
    }
    
}