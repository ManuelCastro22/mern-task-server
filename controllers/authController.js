/* TENDRÁ MÉTODOS RELACIONADOS A UNOS ENDPOINTS */

// Importamos los módelos
const User = require('../models/Users')
// Importamos bcryptjs para hashear las contraseñas
const bcryptjs = require('bcryptjs')
// Importamos express-validator, los resultaos de las validaciones
const { validationResult } = require('express-validator') // importamos la función validationResult
// Importo json web token
const jwt = require('jsonwebtoken')



// Función para autenticar un usuario, es decir cuando inicia sesión
exports.autheticateUser = async (req, res) => { // Como trabaja con express se usará request (lo que el usuario envía) y response (la respuesta)
    // console.log(req.body) // Mostrará en consola el contenido de un json (especificado en el index.js en express.json ) que se encuentre en el body

    // Revisamos si hay errores
    const errors =  validationResult(req)
    if( !errors.isEmpty() ){
        return res.status(400).json({ errors: errors.array() })
    }

    // Haciendo destructuring de los datos enviados en el request
    const { email, pass } = req.body
    
    // Ingresando los datos en la base de datos
    try {

        // Revisando que el usuario ingresado (el email) sea único
        let user = await User.findOne({ email }) // valida si ya existe un usuario con el email que llego en el request, .finOne() es un método de mongoose permite buscar un elemento que cumpla con el parámetro devuelve uno

        // Si no existe un usuario creado con ese email
        if( !user ){
            return res.status(400).json({ msg: 'El usuario no existe' })
        }

        // Revisar el password
        const correctPass = await bcryptjs.compare(pass, user.pass) // Compara la password pasada en el request con la password que está almacenada en el usuario que pasó la validación
        
        // Si la contraseña es incorrecta
        if( !correctPass ){
            return res.status(400).json({ msg: 'Contraseña incorrecta' })
        }

        // Si el usuario y la contraseña están autenticados, entonces
        // Crear el JWT (JSON Web Token)
        const payload = {
            user: {
                id: user.id
            }
        }

        // Firmar el JWT (JSON Web Token)
        jwt.sign(payload, process.env.SECRET, {
            expiresIn: 7200 // 2 horas
        }, (error, token) => {

            if( error ) throw error

            // Mensaje de Confirmación
            res.json({ token })

        }) // Pasamos el payloas, la palabra secreta, configuración, errores

        // Mensaje de Confirmación
        // res.json({ msg: 'Se ha iniciado sesión correctamente' })

    } catch (error) {

        console.log(error)
        res.status(400).send('Hubo un error')

    }

}

// Función para devolver el usuario autenticado
exports.autheticatedUser = async ( req, res ) => {

    try {
        
        // Alamcena la información del usuario en la DB con el id del request
        const user = await User.findById( req.user.id ).select('-pass') // Especificamos que queremos que excluya la contraseña, es decir que no enviará en la respuesta la contraseña

        // como respuesta a la petición devolverá la data del usuario
        res.json({user})

    } catch (error) {
        
        console.log(error)
        res.status(500).json({ msg: 'hubo un error' })

    }

}