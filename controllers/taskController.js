/* TENDRÁ MÉTODOS RELACIONADOS A UNOS ENDPOINTS */

// Importamos los módelos
const Project = require('../models/Projects'),
    Task = require('../models/Task')
// Importamos express-validator, los resultaos de las validaciones
const { validationResult } = require('express-validator') // importamos la función validationResult



// Función pora crear una nueva tarea
exports.createTask = async (req, res) => { // Como trabaja con express se usará request (lo que el usuario envía) y response (la respuesta)
    
    // console.log(req.body) // Mostrará en consola el contenido de un json (especificado en el index.js en express.json ) que se encuentre en el body

    // Revisamos si hay errores en la validación
    const errors =  validationResult(req)
    if( !errors.isEmpty() ){
        return res.status(400).json({ errors: errors.array() })
    }
    
    try {

        // Extraer el proyecto
        const { project } = req.body // id del proyecto

        // Valida si el proyecto existe en la DB
        const projectExist = await Project.findById( project ) // Como parámetro pasamos el projecto que viene en el request

        if( !projectExist ){
            return res.status(404).json( { msg: 'Proyecto no encontrado' } )
        }

        // Verificar que el creador del proyecto sea la misma persona que está autenticada
        if( projectExist.creator.toString() !== req.user.id ) { // Convertimos el valor de creator que se encuentra almacenado como objeto en el módelo a string y lo comparamos con el parámetro con el id del usuario del request
            return res.status(401).json({ msg: "No autorizado" })
        }

        // Creamos la tarea
        const task = new Task(req.body)
        await task.save()

        // Retornamos la respuesta de la DB
        res.json({ task })

    } catch(error) {
        
        console.log(error)
        res.status(400).send('Hubo un error')

    }

}

// Función para obtener las tareas
exports.getTasks = async (req, res) => {

    // console.log(req.body) // Mostrará en consola el contenido de un json (especificado en el index.js en express.json ) que se encuentre en el body

    // Revisamos si hay errores en la validación
    const errors =  validationResult(req)
    if( !errors.isEmpty() ){
        return res.status(400).json({ errors: errors.array() })
    }

    try {

        // Extraer el proyecto
        // const { project } = req.body // id del proyecto
        const { project } = req.query // id del proyecto, con query se obtienen los parametros (params) pasado en el request 

        // Valida si el proyecto existe en la DB
        const projectExist = await Project.findById( project ) // Como parámetro pasamos el projecto que viene en el request

        if( !projectExist ){
            return res.status(404).json( { msg: 'Proyecto no encontrado' } )
        }

        // Obtener las tareas por proyecto
        const tasks = await Task.find({ project })

        // Mensaje de Confirmación
        res.json({ tasks })

    } catch(error) {

        console.log(error)
        res.status(400).send('Hubo un error')

    }

}

exports.updateTask = async (req, res) => {
    
    try {

        // Extraer el proyecto
        const { project, name, state } = req.body // id del proyecto

        // Validar si la tarea existe
        let task = await Task.findById(req.params.id)

        if( !task ) return res.status(404).json({ msg: 'No existe esa tarea' })

        // Valida si el proyecto existe en la DB
        const projectExist = await Project.findById( project ) // Como parámetro pasamos el projecto que viene en el request


        // Verificar que el creador del proyecto sea la misma persona que está autenticada
        if( projectExist.creator.toString() !== req.user.id ) { // Convertimos el valor de creator que se encuentra almacenado como objeto en el módelo a string y lo comparamos con el parámetro con el id del usuario del request
            return res.status(401).json({ msg: "No autorizado" })
        }

        // Crear un objeto con la nueva información
        const newTask = {}

        
        // Valida la nueva información
        newTask.name = name
        newTask.state = state

        // Guardar tarea
        task = await Task.findOneAndUpdate({ _id: req.params.id }, newTask, { new: true }) // Busca por id y actualiza, params: 1. id del proyecto que se va a actualizar, 2. con que se va a actualizar es decir la nueva data, 3 confirmamos

        // Retornamos la respuesta de la DB
        res.json({ task })


    } catch(error) {

        console.log(error)
        res.status(500).send('Hubo un error')

    }

}

exports.deleteTask = async (req, res) => {
    
    try {

        // Extraer el proyecto
        // const { project } = req.body // id del proyecto
        const { project } = req.query // id del proyecto, con query se obtienen los parametros (params) pasado en el request

        // Validar si la tarea existe
        let task = await Task.findById(req.params.id)

        if( !task ) return res.status(404).json({ msg: 'No existe esa tarea' })

        // Valida si el proyecto existe en la DB
        const projectExist = await Project.findById( project ) // Como parámetro pasamos el projecto que viene en el request


        // Verificar que el creador del proyecto sea la misma persona que está autenticada
        if( projectExist.creator.toString() !== req.user.id ) { // Convertimos el valor de creator que se encuentra almacenado como objeto en el módelo a string y lo comparamos con el parámetro con el id del usuario del request
            return res.status(401).json({ msg: "No autorizado" })
        }

        // Eliminar tarea
        await Task.findOneAndDelete({ _id: req.params.id })

        // Retornamos la respuesta de la DB
        res.json({ msg: 'Tarea eliminada' })

    } catch(error) {

        console.log(error)
        res.status(500).send('Hubo un error')

    }

}