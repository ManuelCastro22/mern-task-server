/* TENDRÁ MÉTODOS RELACIONADOS A UNOS ENDPOINTS */

// Importamos los módelos
const User = require('../models/Users')
// Importamos bcryptjs para hashear las contraseñas
const bcryptjs = require('bcryptjs')
// Importamos express-validator, los resultaos de las validaciones
const { validationResult } = require('express-validator') // importamos la función validationResult
// Importo json web token
const jwt = require('jsonwebtoken')


// Para crear un usuario necesita en el header del request tener "Content-Type" con el valor "application/json" (ambas sin comillas)


// Función para crear un nuevo usuario
exports.createUser = async (req, res) => { // Como trabaja con express se usará request (lo que el usuario envía) y response (la respuesta)
    // console.log(req.body) // Mostrará en consola el contenido de un json (especificado en el index.js en express.json ) que se encuentre en el body

    // Revisamos si hay errores en la validación
    const errors =  validationResult(req)
    if( !errors.isEmpty() ){
        return res.status(400).json({ errors: errors.array() })
    }

    // Haciendo destructuring de los datos enviados en el request
    const { email, pass } = req.body
    
    // Ingresando los datos en la base de datos
    try {

        // Revisando que el usuario ingresado (el email) sea único
        let user = await User.findOne({ email }) // valida si ya existe un usuario con el email que llego en el request, .findOne() es un método de mongoose permite buscar un elemento que cumpla con el parámetro y devuelve uno

        // Si ya existe un usuario creado con ese email
        if( user ){
            return res.status(400).json({ msg: 'El usuario ya existe' })
        }

        // Crea el nuevo usuario
        user = new User(req.body)

        // Hashear el password
        const salt = await bcryptjs.genSalt(10) // Un salt nos genera un hash único
        user.pass = await bcryptjs.hash( pass, salt ) // param: password del request, salt

        // Guarda el nuevo usuario
        await user.save()

        // Crear el JWT (JSON Web Token)
        const payload = {
            user: {
                id: user.id
            }
        }

        // Firmar el JWT (JSON Web Token)
        jwt.sign(payload, process.env.SECRET, {
            expiresIn: 7200 // 2 horas
        }, (error, token) => {

            if( error ) throw error

            // Mensaje de Confirmación
            res.json({ token: token })

        }) // Pasamos el payload, la palabra secreta, configuración, errores

        // Mensaje de Confirmación
        // res.json({ msg: 'Usuario creado correctamente' })

    } catch (error) {

        console.log(error)
        res.status(400).send('Hubo un error')

    }

}