/* RUTAS PARA MANEJAR TAREAS */

// Importamos express
const express = require('express'),
    router = express.Router()
// Importamos los controllers
const taskController = require('../controllers/taskController')
// Importamos la autenticación con el middleware
const auth = require('../middleware/auth')
// Importamos express-validator
const { check } = require('express-validator') // importamos la función check



// Creando los endpoints
// Crear una tarea
// api/tasks
router.post('/',
    auth,
    [
        check('name', 'El nombre es obligatorio').not().isEmpty(),
        check('project', 'El id del proyecto es obligatorio').not().isEmpty()
    ],
    taskController.createTask
)

router.get('/',
    auth,
    [
        check('project', 'El id del proyecto es obligatorio').not().isEmpty()
    ],
    taskController.getTasks
)

router.put('/:id',
    auth,
    taskController.updateTask
)

router.delete('/:id',
    auth,
    taskController.deleteTask
)

module.exports = router // Exportamos el router creado con express