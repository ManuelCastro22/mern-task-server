/* RUTAS PARA MANEJAR PROYECTOS */

// Importamos express
const express = require('express'),
    router = express.Router()
// Importamos los controllers
const projectController = require('../controllers/projectController')
// Importamos la autenticación con el middleware
const auth = require('../middleware/auth')
// Importamos express-validator
const { check } = require('express-validator') // importamos la función check



// Creando los endpoints
// api/projects
router.post('/',
    auth, // Primero válida que este autenticado, si lo está pasará al otro middleware
    [
        check('name', 'El nombre del proyecto es obligatorio').not().isEmpty()
    ], // Validamos con express validator
    projectController.createProject // Crear un proyecto
) // Cuando envien un post hacía la url principal

router.get('/',
    auth, // Primero válida que este autenticado, si lo está pasará al otro middleware
    projectController.getProjects // Obtener los proyectos del usuario actual
) // Cuando envien un get hacía la url principal

router.put('/:id', // Recibe el id del proyecto a actualizar
    auth, // Primero válida que este autenticado, si lo está pasará al otro middleware
    [
        check('name', 'El nombre del proyecto es obligatorio').not().isEmpty()
    ], // Validamos con express validator
    projectController.updateProject // Actualizar un proyecto vía id del usuario actual
) // Cuando envien un put con id hacía la url principal

router.delete('/:id', // Recibe el id del proyecto a eliminar
    auth, // Primero válida que este autenticado, si lo está pasará al otro middleware
    projectController.deleteProject // Eliminar un proyecto vía id del usuario actual
) // Cuando envien un delete con id hacía la url principal

module.exports = router // Exportamos el router creado con express