/* RUTAS PARA CREAR USUARIOS */

// Importamos express
const express = require('express')
const router = express.Router()
// Importamos los controllers
const userController = require('../controllers/userController')
// Importamos express-validator
const { check } = require('express-validator') // importamos la función check



// Creando los endpoints
// Crear un usuario
// api/users
router.post('/',
    [
        check('name', 'El nombre es obligatorio').not().isEmpty(),
        check('email', 'Agrega un email válido').isEmail(),
        check('pass', 'El password debe de ser de mínimo 6 caracteres').isLength({ min: 6 })
    ],
    userController.createUser )

module.exports = router // Exportamos el router creado con express