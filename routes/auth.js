/* RUTAS PARA AUTENTICAR USUARIOS (INICIAR SESIÓN) */

// Importamos express
const express = require('express')
const router = express.Router()
// Importamos los controllers
const authController = require('../controllers/authController')
// Importamos la autenticación con el middleware
const auth = require('../middleware/auth')
// Importamos express-validator
const { check } = require('express-validator') // importamos la función check



// Creando los endpoints
// Iniciar sesión
// api/auth
router.post('/',
    [
        check('email', 'Agrega un email válido').isEmail(),
        check('pass', 'El password debe de ser de mínimo 6 caracteres').isLength({ min: 6 })
    ], authController.autheticateUser )

// Obtiene el usuario autenticado
router.get('/',
    auth, // autentica que sea un usuario autenticado
    authController.autheticatedUser
)

module.exports = router // Exportamos el router creado con express