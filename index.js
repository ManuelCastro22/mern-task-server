// Importamos express para crear el servidor
const express = require('express')
// Importamos la función para conectarse a la DB en el archivo de configuración de la Data base
const conectDB = require('./config/bd')
// Importar cors
const cors = require('cors')



// Crear el servidor
const app = express()

// Conectar la DB
conectDB()

// Habilitar cors
app.use(cors())

// Habilitar express.json para poder usar los datos que el usuario ingrese
app.use( express.json({
    extended: true
}) )

// Puerto de la app, Validamos si existe la variable de entorno o le asigna el puerto 4000
const port = process.env.PORT || 4000

// Importar rutas
app.use('/api/users', require('./routes/users')) // Aquí le decimos que va a pasa cuando se pasé la url /api/users
app.use('/api/auth', require('./routes/auth')) // Aquí le decimos que va a pasa cuando se pasé la url /api/auth para autenticar usuarios
app.use('/api/projects', require('./routes/projects')) // Aquí le decimos que va a pasar cuando se pasé la url /api/projects para manejar los proyectos
app.use('/api/tasks', require('./routes/tasks')) // Aquí le decimos que va a pasar cuando se pasé la url /api/tasks para manejar las tareas

// Definir la página principal
// app.get('/', (req, res) => {
//     res.send('Hola Doggy')
// })

// Arrancar la app
app.listen(port, '0.0.0.0', () => {
    console.log(`El servidor está funcionando en el puerto ${port}`)
})